import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import authReducer from './features/auth/authSlice';
import manhwaReducer from './features/manhwa/manhwaSlice';

export default configureStore({
	reducer: {
		auth: authReducer,
		manhwa: manhwaReducer,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});
