import {
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	signOut,
	updateProfile,
	updatePassword,
} from 'firebase/auth';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { auth } from '../../../firebase/config';

const initialState = {
	username: '',
	email: '',
	password: '',
	confirmPassword: '',
	error: '',
	token: '',
};

export const registerUser = createAsyncThunk(
	'auth/registerUser',
	async (credential) => {
		const { username, email, password } = credential;
		try {
			const userCredential = await createUserWithEmailAndPassword(
				auth,
				email,
				password
			);
			updateProfile(auth.currentUser, {
				displayName: username,
			});
			return { userCredential, displayName: username };
		} catch (error) {
			throw error;
		}
	}
);

export const loginUser = createAsyncThunk(
	'auth/loginUser',
	async (credential) => {
		const { email, password } = credential;
		try {
			const userCredential = await signInWithEmailAndPassword(
				auth,
				email,
				password
			);
			return userCredential;
		} catch (error) {
			throw error;
		}
	}
);

export const logoutUser = createAsyncThunk('auth/logoutUser', async () => {
	try {
		await signOut(auth);
	} catch (error) {
		throw error;
	}
});

export const editUser = createAsyncThunk(
	'auth/editUser',
	async (credential, { dispatch }) => {
		try {
			await updateProfile(auth.currentUser, {
				displayName: credential.username,
			});
			if (credential.password) {
				try {
					await updatePassword(auth.currentUser, credential.password);
				} catch (error) {
					throw error;
				}
			}
			return { message: 'Profile changed successfully', credential };
		} catch (error) {
			throw error;
		}
	}
);

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		setToken: (state, action) => {
			if (action.payload) {
				state.token = action.payload.uid;
				state.email = action.payload.email;
				state.username = action.payload.displayName;
			}
		},
	},
	extraReducers: (builder) => {
		builder.addCase(registerUser.fulfilled, (state, action) => {
			state.error = '';
			state.token = action.payload.userCredential.user.getIdToken();
			state.username = action.payload.displayName;
			state.email = action.payload.userCredential.user.email;
		});
		builder.addCase(registerUser.pending, (state, _action) => {
			state.error = '';
		});
		builder.addCase(registerUser.rejected, (state, action) => {
			state.error = action.error.code.replace(/-/g, ' ');
		});
		builder.addCase(loginUser.fulfilled, (state, action) => {
			state.error = '';
			state.token = action.payload.user.getIdToken();
			state.username = action.payload.user.displayName;
			state.email = action.payload.user.email;
		});
		builder.addCase(loginUser.pending, (state, _action) => {
			state.error = '';
		});
		builder.addCase(loginUser.rejected, (state, action) => {
			state.error = action.error.code.replace(/-/g, ' ');
		});
		builder.addCase(logoutUser.fulfilled, (state, action) => {
			state.error = '';
			state.token = '';
			state.username = '';
			state.password = '';
		});
		builder.addCase(editUser.fulfilled, (state, action) => {
			state.username = action.payload.credential.username;
		});
	},
});

export const { setToken } = authSlice.actions;

export default authSlice.reducer;
