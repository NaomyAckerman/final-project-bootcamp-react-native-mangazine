import {
	collection,
	addDoc,
	query,
	where,
	getDocs,
	deleteDoc,
	doc,
	writeBatch,
} from 'firebase/firestore';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { MANHWA } from '../../../api';
import { db } from '../../../firebase/config';
import { util } from '../../../utils';

const initialState = {
	latest: [],
	popular: {
		weekly: [],
		monthly: [],
		alltime: [],
		today: [],
	},
	genre: [],
	listGenre: [],
	list: [],
	listManhwa: [],
	favorite: [],
	favoriteDetail: {},
	detail: {},
	read: {},
};

export const getNewRelease = createAsyncThunk(
	'manhwa/getNewRelease',
	async ({ page = 1, count = 30, type }) => {
		try {
			const response = await MANHWA.get(`/latest?page=${page}`);
			return { type, data: response.data.manhwas.splice(0, count) };
		} catch (error) {
			throw error;
		}
	}
);
export const getPopular = createAsyncThunk(
	'manhwa/getList',
	async ({ date = '/today' }) => {
		try {
			const response = await MANHWA.get(`/popular${date}`);
			return response.data;
		} catch (error) {
			throw error;
		}
	}
);
export const getAll = createAsyncThunk(
	'manhwa/getAll',
	async ({ page = 1, count = 30, type }) => {
		try {
			const response = await MANHWA.get(`/all?page=${page}`);
			return { type, data: response.data.manhwas.splice(0, count) };
		} catch (error) {
			throw error;
		}
	}
);
export const getFavorite = createAsyncThunk(
	'manhwa/getFavorite',
	async ({ uid, title }) => {
		try {
			let favorite = [];
			const q = !title
				? query(collection(db, 'favorite'), where('uid', '==', uid))
				: query(
						collection(db, 'favorite'),
						where('uid', '==', uid),
						where('title', '==', title)
				  );
			const querySnapshot = await getDocs(q);
			querySnapshot.forEach((doc) => {
				favorite.push({ id: doc.id, ...doc.data() });
			});
			return { type: !title ? 'multi' : 'single', favorite };
		} catch (error) {
			throw error;
		}
	}
);
export const setFavorite = createAsyncThunk(
	'manhwa/setFavorite',
	async (payload, { dispatch }) => {
		const { uid, title } = payload;
		try {
			const docRef = await addDoc(collection(db, 'favorite'), payload);
			dispatch(getFavorite({ uid, title }));
			dispatch(getFavorite({ uid }));
			return { id: docRef.id, message: 'Successfully liked comics' };
		} catch (error) {
			throw error;
		}
	}
);
export const deleteFavorite = createAsyncThunk(
	'manhwa/deleteFavorite',
	async ({ id, uid, title }, { dispatch }) => {
		try {
			let message;
			const result = await deleteDoc(doc(db, 'favorite', id));
			if (title) {
				dispatch(getFavorite({ uid, title }));
				message = 'Successfully unliked comics';
			} else {
				message = 'Successfully delete comics';
			}
			dispatch(getFavorite({ uid }));
			return { result, message };
		} catch (error) {
			throw error;
		}
	}
);
export const getDetail = createAsyncThunk(
	'manhwa/getDetail',
	async ({ endpoint, uid }, { dispatch }) => {
		try {
			const result = await MANHWA.get(`/detail/${endpoint}`);
			dispatch(getFavorite({ uid, title: result.data.manhwa.title }));
			return result.data.manhwa;
		} catch (error) {
			throw error;
		}
	}
);
export const getGenre = createAsyncThunk(
	'manhwa/getGenre',
	async ({ endpoint, page = 1 }) => {
		try {
			const result = await MANHWA.get(
				`/genre/${!endpoint ? '' : `${endpoint}?page=${page}`}`
			);
			return {
				type: !endpoint ? 'genre' : 'genreList',
				data: result.data,
			};
		} catch (error) {
			throw error;
		}
	}
);
export const getRead = createAsyncThunk(
	'manhwa/getRead',
	async ({ endpoint }, { getState }) => {
		try {
			const {
				favoriteDetail: { id, title },
			} = getState().manhwa;
			const result = await MANHWA.get(`/read/${endpoint}`);
			if (id !== undefined) {
				const batch = writeBatch(db);
				const manhwaRef = doc(db, 'favorite', id);
				batch.update(manhwaRef, {
					latest_read: util.replaceChapter(result.data.chapter.title, title),
					latest_read_endpoint: endpoint,
				});
				await batch.commit();
			}
			return result.data.chapter;
		} catch (error) {
			throw error;
		}
	}
);

export const manhwaSlice = createSlice({
	name: 'manhwa',
	initialState,
	reducers: {
		reset: (state, action) => {
			state[action.payload.key] = action.payload.value;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(getNewRelease.fulfilled, (state, action) => {
			const { type, data } = action.payload;
			!type
				? (state.latest = data)
				: (state.listManhwa = [...state.listManhwa, ...data]);
		});
		builder.addCase(getAll.fulfilled, (state, action) => {
			const { type, data } = action.payload;
			!type
				? (state.list = data)
				: (state.listManhwa = [...state.listManhwa, ...data]);
		});
		builder.addCase(getPopular.fulfilled, (state, action) => {
			const { message, manhwas } = action.payload;
			switch (message) {
				case 'Popular Weekly':
					state.popular.weekly = manhwas;
					break;
				case 'Popular Monthly':
					state.popular.monthly = manhwas;
					break;
				case 'Popular Alltime':
					state.popular.alltime = manhwas;
					break;
				default:
					state.popular.today = manhwas;
					break;
			}
		});
		builder.addCase(getFavorite.fulfilled, (state, action) => {
			const { type, favorite } = action.payload;
			type === 'single'
				? (state.favoriteDetail = favorite.length ? favorite[0] : {})
				: (state.favorite = favorite);
		});
		builder.addCase(getGenre.fulfilled, (state, action) => {
			const { type, data } = action.payload;
			type === 'genre'
				? (state.genre = data.genres)
				: (state.listGenre = data.manhwas);
		});
		builder.addCase(getDetail.fulfilled, (state, action) => {
			state.detail = action.payload;
		});
		builder.addCase(getRead.fulfilled, (state, action) => {
			state.read = action.payload;
		});
		// builder.addCase(getFavorite.pending, (state, action) => {
		// 	state.favorite = [];
		// });
		// builder.addCase(getDetail.pending, (state, action) => {
		// 	state.detail = {};
		// });
		// builder.addCase(getDetail.rejected, (state, action) => {
		// 	state.detail = {};
		// });
		// builder.addCase(getRead.pending, (state, action) => {
		// 	state.read = {};
		// });
		// builder.addCase(getRead.rejected, (state, action) => {
		// 	state.read = {};
		// });
	},
});

export const { reset } = manhwaSlice.actions;

export default manhwaSlice.reducer;
