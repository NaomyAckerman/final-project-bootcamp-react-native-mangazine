import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthStack from './navigator/AuthStack';

const index = () => {
	return (
		<NavigationContainer>
			<AuthStack />
		</NavigationContainer>
	);
};

export default index;
