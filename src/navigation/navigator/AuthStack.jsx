import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import {
	WelcomeScreen,
	LoginScreen,
	RegisterScreen,
	MangaListScreen,
	DetailScreen,
	ReadScreen,
} from '../../screens';
import HomeTab from './HomeTab';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../../firebase/config';
import { setToken } from '../../redux/features/auth/authSlice';

const Stack = createStackNavigator();

const AuthStack = () => {
	const dispatch = useDispatch();
	const { token } = useSelector((state) => state.auth);

	onAuthStateChanged(auth, (user) => {
		if (user) {
			dispatch(setToken(user));
		} else {
			dispatch(setToken(''));
		}
	});

	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			{!token ? (
				<>
					<Stack.Screen name="Welcome" component={WelcomeScreen} />
					<Stack.Screen name="Login" component={LoginScreen} />
					<Stack.Screen name="Register" component={RegisterScreen} />
				</>
			) : (
				<>
					<Stack.Screen name="TabBottom" component={HomeTab} />
					<Stack.Screen
						name="List"
						component={MangaListScreen}
						options={({ route }) => ({
							title: `Manhwa ${route.params.part}`,
							headerShown: true,
						})}
					/>
					<Stack.Screen name="Detail" component={DetailScreen} />
					<Stack.Screen name="Read" component={ReadScreen} />
				</>
			)}
		</Stack.Navigator>
	);
};

export default AuthStack;
