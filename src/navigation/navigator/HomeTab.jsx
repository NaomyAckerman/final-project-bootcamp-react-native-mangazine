import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FavoriteScreen, HomeScreen, ProfileScreen } from '../../screens';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { colors } from '../../themes/colors';

const Tab = createBottomTabNavigator();

export default function HomeTab() {
	return (
		<Tab.Navigator
			screenOptions={({ route }) => ({
				headerShown: false,
				tabBarIcon: ({ color, size }) => {
					if (route.name === 'Home') {
						return <Ionicons name="home-outline" size={size} color={color} />;
					} else if (route.name === 'Favorite') {
						return <AntDesign name="hearto" size={size} color={color} />;
					} else {
						return <AntDesign name="user" size={size} color={color} />;
					}
				},
			})}
			tabBarOptions={{
				activeTintColor: colors.blue,
				inactiveTintColor: colors.black,
				style: { height: 60 },
				labelStyle: { marginBottom: 7 },
			}}
			sceneContainerStyle={{ backgroundColor: colors.white }}
		>
			<Tab.Screen name="Home" component={HomeScreen} />
			<Tab.Screen name="Favorite" component={FavoriteScreen} />
			<Tab.Screen name="Profile" component={ProfileScreen} />
		</Tab.Navigator>
	);
}
