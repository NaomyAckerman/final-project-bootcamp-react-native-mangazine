import welcomeImage from './images/welcome.png';
import loginImage from './images/login.png';
import registerImage from './images/register.png';
import japanImage from './images/japan.png';
import chinaImage from './images/china.png';
import koreaImage from './images/korea.png';
import nodataImage from './images/nodata.png';
export {
	welcomeImage,
	loginImage,
	registerImage,
	japanImage,
	chinaImage,
	koreaImage,
	nodataImage,
};
