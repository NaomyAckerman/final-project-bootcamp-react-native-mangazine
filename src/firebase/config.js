import { initializeApp, getApps } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
	apiKey: 'AIzaSyDqDvSMNa0l3AKMnbasn6c2RlcI18J9Y_M',
	authDomain: 'mangazine-c337d.firebaseapp.com',
	projectId: 'mangazine-c337d',
	storageBucket: 'mangazine-c337d.appspot.com',
	messagingSenderId: '520020448336',
	appId: '1:520020448336:web:90a2ceab8294403e026d55',
	measurementId: 'G-1FL3HBPNFG',
};

// Initialize Firebase
if (!getApps().length) {
	initializeApp(firebaseConfig);
}

export const auth = getAuth();
export const db = getFirestore();
