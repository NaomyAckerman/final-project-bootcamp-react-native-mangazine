import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors } from '../../themes/colors';

export default function ContentSynopsisDetail({ detail }) {
	return (
		<>
			<View style={styles.synopsisWrapper}>
				<Text style={styles.textTitle}>Synopsis</Text>
				<Text style={styles.synopsisText}>{detail.synopsis}</Text>
			</View>
		</>
	);
}

const styles = StyleSheet.create({
	synopsisWrapper: {
		marginHorizontal: 25,
		marginTop: 25,
		elevation: 3,
		backgroundColor: colors.white,
		paddingHorizontal: 25,
		paddingVertical: 20,
		borderRadius: 25,
	},
	synopsisText: {
		fontSize: 15,
		color: colors.secondary,
		lineHeight: 24,
		marginTop: 10,
	},
	textTitle: {
		fontSize: 18,
		color: colors.darkBlue,
		fontWeight: 'bold',
		marginBottom: 5,
	},
});
