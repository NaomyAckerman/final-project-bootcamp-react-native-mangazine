import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../themes/colors';
import ButtonChapterComponent from '../button/chapter';
import GapComponent from '../gap';

export default function ContentChapterDetail({ chapter, onPress }) {
	return (
		<View style={styles.chapterWrapper}>
			<Text style={styles.textTitle}>Chapter List</Text>
			<FlatList
				data={chapter}
				renderItem={({ item, index }) => {
					return (
						<>
							<GapComponent height={15} key={index.toString()} />
							<ButtonChapterComponent
								outline={index === 0 ? false : true}
								news={index === 0 ? true : false}
								info={item}
								onPress={() => {
									onPress({ item });
								}}
							/>
						</>
					);
				}}
				keyExtractor={(_item, index) => index.toString()}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	chapterWrapper: {
		marginHorizontal: 25,
		marginTop: 10,
		marginBottom: 20,
		paddingHorizontal: 25,
		paddingVertical: 20,
	},
	textTitle: {
		fontSize: 18,
		color: colors.darkBlue,
		fontWeight: 'bold',
		marginBottom: 5,
	},
});
