import React, { useRef, useState } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { LinearGradient } from 'expo-linear-gradient';
import { colors } from '../../themes/colors';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import {
	Dimensions,
	Image,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import { chinaImage, japanImage, koreaImage } from '../../assets';
import { util } from '../../utils';

const { width: viewportWidth, height: viewportHeight } =
	Dimensions.get('window');
function wp(percentage) {
	const value = (percentage * viewportWidth) / 100;
	return Math.round(value);
}

const slideHeight = viewportHeight * 0.27;
const entryBorderRadius = 25;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(1);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const SliderEntry = ({ item, navigation }) => {
	const { title, latest_chapter, thumbnail, type, colored, rating } = item;
	return (
		<View style={styles.slideInnerContainer}>
			<View style={styles.imageContainer}>
				<TouchableOpacity
					activeOpacity={0.7}
					onPress={() =>
						navigation.navigate('Detail', { endpoint: item.endpoint })
					}
				>
					<LinearGradient
						colors={['rgba(0,0,0,0.1)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,0.8)']}
						style={styles.background}
					/>
					<Image source={{ uri: thumbnail }} style={styles.image} />
				</TouchableOpacity>
				<View style={styles.leftTopInfo}>
					<Image
						source={
							type.toLowerCase() === 'manhwa'
								? koreaImage
								: type.toLowerCase() === 'manhua'
								? chinaImage
								: japanImage
						}
						style={styles.type}
					/>
				</View>
				<View style={styles.rightTopInfo}>
					{colored && (
						<Ionicons
							name="color-palette"
							size={24}
							color={colors.lightBlue}
							style={styles.colored}
						/>
					)}
				</View>
				<View style={styles.leftBotomInfo}>
					<Text style={styles.title}>{util.sliceText(title, 31)}</Text>
					<View style={styles.rating}>
						<AntDesign name="star" size={15} color="#FFCD3C" />
						<Text style={styles.ratingText}>{rating}</Text>
					</View>
					<Text style={styles.chp}>{latest_chapter}</Text>
				</View>
			</View>
		</View>
	);
};

export default function ContentListLatest({ item, navigation }) {
	const carouselRef = useRef(null);
	const [activeDotCarousel, setActiveDotCarousel] = useState(0);

	return (
		<View>
			<Carousel
				ref={carouselRef}
				data={item}
				renderItem={({ item }) => (
					<SliderEntry item={item} navigation={navigation} />
				)}
				sliderWidth={sliderWidth}
				itemWidth={itemWidth}
				firstItem={0}
				inactiveSlideScale={0.95}
				inactiveSlideOpacity={0.6}
				// inactiveSlideShift={20}
				contentContainerCustomStyle={styles.sliderContentContainer}
				loop={true}
				loopClonesPerSide={2}
				autoplay={true}
				autoplayDelay={5000}
				autoplayInterval={5000}
				onSnapToItem={(index) => setActiveDotCarousel(index)}
				keyExtractor={(_item, index) => index.toString()}
			/>
			<Pagination
				dotsLength={item.length}
				activeDotIndex={activeDotCarousel}
				containerStyle={styles.paginationContainer}
				dotColor={colors.blue}
				inactiveDotColor={colors.white}
				dotStyle={styles.paginationDot}
				inactiveDotStyle={styles.inactivePaginationDot}
				inactiveDotOpacity={1}
				inactiveDotScale={0.98}
				carouselRef={carouselRef}
				tappableDots={!!carouselRef}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	sliderContentContainer: {
		paddingTop: 10,
		paddingBottom: 10,
	},
	slideInnerContainer: {
		width: itemWidth,
		height: slideHeight,
		paddingHorizontal: itemHorizontalMargin,
	},
	imageContainer: {
		overflow: 'hidden',
		position: 'relative',
		borderRadius: entryBorderRadius,
	},
	background: {
		position: 'absolute',
		zIndex: 1,
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		borderRadius: entryBorderRadius,
	},
	image: {
		height: '100%',
		width: '100%',
		resizeMode: 'cover',
	},
	leftBotomInfo: {
		position: 'absolute',
		bottom: 10,
		left: 10,
	},
	leftTopInfo: {
		position: 'absolute',
		top: 10,
		left: 10,
	},
	rightTopInfo: {
		position: 'absolute',
		top: 10,
		right: 10,
	},
	title: {
		fontSize: 20,
		marginLeft: 10,
		marginBottom: 40,
		letterSpacing: 0.7,
		color: colors.white,
		fontWeight: 'bold',
	},
	type: {
		width: 40,
		height: 30,
		marginLeft: 10,
		marginTop: 10,
		borderRadius: 7,
	},
	colored: {
		marginTop: 10,
		marginRight: 10,
	},
	rating: {
		flexDirection: 'row',
		position: 'absolute',
		alignItems: 'center',
		bottom: 10,
		left: 10,
	},
	ratingText: {
		color: colors.white,
		fontSize: 15,
		marginLeft: 5,
	},
	chp: {
		position: 'absolute',
		color: colors.white,
		width: '100%',
		marginLeft: 20,
		fontSize: 15,
		bottom: 10,
		left: 35,
	},
	paginationContainer: {
		paddingBottom: 20,
		paddingTop: 0,
	},
	paginationDot: {
		width: 12,
		height: 12,
		borderRadius: 10,
	},
	inactivePaginationDot: {
		width: 10,
		height: 10,
		borderRadius: 5,
		marginHorizontal: 2,
		borderWidth: 1,
	},
});
