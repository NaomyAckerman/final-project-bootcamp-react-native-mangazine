import React from 'react';
import {
	FlatList,
	Image,
	StyleSheet,
	Text,
	View,
	Dimensions,
	TouchableOpacity,
} from 'react-native';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import GapComponent from '../gap';
import { colors } from '../../themes/colors';
import { util } from '../../utils';
import { LinearGradient } from 'expo-linear-gradient';
import { chinaImage, japanImage, koreaImage } from '../../assets';

const contentCol = 3;
const { width } = Dimensions.get('window');
const itemWidth = (width - 70) / contentCol;

const RenderContent = ({ item, section, navigation }) => {
	return (
		<View style={[styles.item, section.bgSecondary && styles.bgSecondary]}>
			<View style={styles.infoWrapper}>
				<TouchableOpacity
					activeOpacity={0.8}
					onPress={() =>
						navigation.navigate('Detail', { endpoint: item.endpoint })
					}
				>
					<LinearGradient
						colors={['rgba(0,0,0,0.1)', 'rgba(0,0,0,0.2)', 'rgba(0,0,0,0.6)']}
						style={styles.background}
					/>
					<Image source={{ uri: item.thumbnail }} style={styles.thumbnail} />
				</TouchableOpacity>
				<Image
					source={
						item.type.toLowerCase() === 'manhwa'
							? koreaImage
							: item.type.toLowerCase() === 'manhua'
							? chinaImage
							: japanImage
					}
					style={styles.type}
				/>
				{item.colored && (
					<Ionicons
						name="color-palette"
						size={24}
						color={colors.lightBlue}
						style={styles.colored}
					/>
				)}
				<View style={styles.rating}>
					<AntDesign name="star" size={15} color="#FFCD3C" />
					<Text style={styles.ratingText}>{item.rating}</Text>
				</View>
			</View>
			<Text style={styles.title}>{util.sliceText(item.title, 16)}</Text>
			<Text style={styles.chp}>{item.latest_chapter}</Text>
		</View>
	);
};

export default function ContentListAll({ item, section, navigation }) {
	return (
		<FlatList
			data={item}
			renderItem={({ item }) => (
				<RenderContent item={item} section={section} navigation={navigation} />
			)}
			keyExtractor={(_item, index) => index.toString()}
			ItemSeparatorComponent={() => <GapComponent height={13} />}
			numColumns={contentCol}
			columnWrapperStyle={styles.itemWrapper}
			style={styles.container}
		/>
	);
}

const styles = StyleSheet.create({
	container: {
		paddingTop: 10,
		paddingBottom: 20,
	},
	itemWrapper: {
		paddingHorizontal: 20,
		justifyContent: 'space-between',
	},
	item: {
		width: itemWidth,
	},
	thumbnail: {
		width: itemWidth,
		height: 175,
		borderRadius: 25,
	},
	title: {
		color: colors.black,
		fontWeight: '700',
		marginTop: 8,
		marginBottom: 3,
	},
	chp: {
		color: colors.secondary,
	},
	bgSecondary: {
		backgroundColor: colors.lightBlue,
	},
	infoWrapper: {
		position: 'relative',
	},
	background: {
		position: 'absolute',
		zIndex: 1,
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		borderRadius: 25,
	},
	type: {
		zIndex: 2,
		width: 30,
		height: 20,
		position: 'absolute',
		borderRadius: 7,
		top: 10,
		left: 15,
	},
	colored: {
		zIndex: 2,
		position: 'absolute',
		bottom: 10,
		right: 10,
	},
	rating: {
		flexDirection: 'row',
		zIndex: 2,
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
		bottom: 10,
		left: 10,
	},
	ratingText: {
		color: colors.white,
		fontSize: 15,
		marginLeft: 3,
	},
});
