import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../themes/colors';
import GapComponent from '../gap';

export default function ContentGenreDetail({ data }) {
	return (
		<View style={styles.genreWrapper}>
			<Text style={styles.textTitle}>Genre</Text>
			<FlatList
				data={data}
				renderItem={({ item }) => (
					<Text style={styles.genreText}>{item.name}</Text>
				)}
				style={styles.genre}
				ItemSeparatorComponent={() => <GapComponent width={10} />}
				keyExtractor={(_item, index) => index.toString()}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	textTitle: {
		fontSize: 18,
		color: colors.darkBlue,
		fontWeight: 'bold',
		marginBottom: 5,
	},
	genreWrapper: {
		marginHorizontal: 25,
		marginTop: 30,
		elevation: 3,
		backgroundColor: colors.white,
		paddingHorizontal: 25,
		paddingVertical: 20,
		borderRadius: 25,
	},
	genre: {
		flexWrap: 'wrap',
		flexDirection: 'row',
	},
	genreText: {
		fontSize: 15,
		marginRight: 10,
		color: colors.white,
		backgroundColor: colors.blue,
		paddingVertical: 7,
		paddingHorizontal: 20,
		borderRadius: 50,
		textAlign: 'center',
		textAlignVertical: 'center',
		marginTop: 10,
	},
});
