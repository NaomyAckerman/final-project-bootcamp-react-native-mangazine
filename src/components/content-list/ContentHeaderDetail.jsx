import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import {
	ActivityIndicator,
	Image,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import { colors } from '../../themes/colors';
import BackComponent from '../back';
import { AntDesign } from '@expo/vector-icons';

export default function ContentHeaderDetail({
	navigation,
	detail,
	favorite,
	onFavorite,
	loading,
}) {
	return (
		<>
			<View style={styles.thumbnailWrapper}>
				<View style={styles.buttonBackWrapper}>
					<BackComponent
						border={false}
						color={colors.white}
						onPress={() => {
							navigation.goBack();
						}}
					/>
				</View>
				<LinearGradient
					colors={['rgba(0,0,0,0.1)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,0.8)']}
					style={styles.background}
				/>
				<Image
					source={{
						uri: detail.thumbnail,
					}}
					style={styles.thumbnail}
				/>
				<Text style={styles.title} numberOfLines={2}>
					{detail.title}
				</Text>
				{detail.title && (
					<TouchableOpacity
						style={styles.favoriteWrapper}
						activeOpacity={0.7}
						onPress={onFavorite}
						disabled={loading}
					>
						{loading ? (
							<ActivityIndicator size="small" color={colors.white} />
						) : (
							<AntDesign
								name={favorite.title ? 'heart' : 'hearto'}
								size={24}
								color="white"
							/>
						)}
					</TouchableOpacity>
				)}
			</View>
			<Text style={styles.detailTitle}>{detail.title}</Text>
		</>
	);
}

const styles = StyleSheet.create({
	thumbnailWrapper: {
		position: 'relative',
		overflow: 'hidden',
		borderBottomEndRadius: 40,
		borderBottomStartRadius: 40,
	},
	thumbnail: {
		height: 320,
	},
	buttonBackWrapper: {
		position: 'absolute',
		zIndex: 2,
		top: 40,
		left: 20,
	},
	background: {
		zIndex: 1,
		position: 'absolute',
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
	},
	title: {
		position: 'absolute',
		zIndex: 2,
		bottom: 60,
		left: 30,
		fontSize: 20,
		fontWeight: 'bold',
		letterSpacing: 0.5,
		color: colors.white,
		lineHeight: 28,
		textAlign: 'left',
		width: 340,
	},
	favoriteWrapper: {
		position: 'absolute',
		zIndex: 4,
		bottom: 60,
		right: 30,
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.heart,
		borderRadius: 50,
	},
	detailTitle: {
		fontSize: 17,
		textAlign: 'center',
		marginTop: 25,
		lineHeight: 22,
	},
});
