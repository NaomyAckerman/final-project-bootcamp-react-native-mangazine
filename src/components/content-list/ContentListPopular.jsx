import React from 'react';
import {
	FlatList,
	Image,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import GapComponent from '../gap';
import { colors } from '../../themes/colors';
import { util } from '../../utils';

const RenderContent = ({ item, navigation }) => (
	<View style={styles.content}>
		<TouchableOpacity
			activeOpacity={0.8}
			onPress={() => navigation.navigate('Detail', { endpoint: item.endpoint })}
		>
			<Image source={{ uri: item.thumbnail }} style={styles.thumbnail} />
		</TouchableOpacity>
		<Text style={styles.title}>{util.sliceText(item.title, 20)}</Text>
		<Text style={styles.genre}>
			{util.sliceText(util.generateGenre(item.genre), 20)}
		</Text>
	</View>
);

export default function ContentListPopular({ item, section, navigation }) {
	return (
		<FlatList
			data={item}
			renderItem={({ item }) => (
				<RenderContent item={item} navigation={navigation} />
			)}
			keyExtractor={(_item, index) => index.toString()}
			horizontal={true}
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={[
				styles.container,
				section.bgSecondary && styles.bgSecondary,
			]}
			ItemSeparatorComponent={() => <GapComponent width={15} />}
		/>
	);
}

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 20,
	},
	content: {
		paddingTop: 10,
		paddingBottom: 20,
	},
	thumbnail: {
		height: 170,
		width: 135,
		borderRadius: 25,
	},
	title: {
		color: colors.black,
		fontWeight: '700',
		marginTop: 8,
		marginBottom: 3,
	},
	genre: {
		color: colors.secondary,
	},
	bgSecondary: {
		backgroundColor: colors.lightBlue,
	},
});
