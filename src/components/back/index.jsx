import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { colors } from '../../themes/colors';

export default function index({ border = true, color = colors.blue, ...rest }) {
	return (
		<TouchableOpacity
			style={[styles.backWrapper, !border ? styles.backNoBorder : '']}
			{...rest}
		>
			<Entypo name="chevron-left" size={24} color={color} />
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	backWrapper: {
		width: 40,
		height: 40,
		borderWidth: 2,
		borderRadius: 25,
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: colors.blue,
	},
	backNoBorder: {
		borderWidth: 0,
	},
});
