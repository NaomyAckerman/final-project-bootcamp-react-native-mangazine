import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { nodataImage } from '../../assets';
import { colors } from '../../themes/colors';

const { height } = Dimensions.get('window');

export default function index({ title }) {
	return (
		<View style={styles.container}>
			<Image source={nodataImage} style={styles.image} />
			<Text style={styles.text}>{title}</Text>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		height: height - 210,
		justifyContent: 'center',
		alignItems: 'center',
	},
	image: {
		height: 120,
		width: 120,
	},
	text: {
		fontSize: 16,
		fontWeight: '500',
		color: colors.secondary,
	},
});
