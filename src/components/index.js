import ButtonComponent from './button';
import ButtonChapterComponent from './button/chapter';
import BackComponent from './back';
import InputComponent from './input';
import ErrorInputComponent from './error-input';
import GapComponent from './gap';
import HeaderComponent from './header';
import NotifComponent from './notification';
import NodataComponent from './nodata';
import NotifSuccessComponent from './notification/Success';
import HeaderListHome from './header-list/HeaderListHome';
import ContentListLatest from './content-list/ContentListLatest';
import ContentListPopular from './content-list/ContentListPopular';
import ContentListAll from './content-list/ContentListAll';
import ContentHeaderDetail from './content-list/ContentHeaderDetail';
import ContentGenreDetail from './content-list/ContentGenreDetail';
import ContentSynopsisDetail from './content-list/ContentSynopsisDetail';
import ContentChapterDetail from './content-list/ContentChapterDetail';
export {
	ButtonComponent,
	GapComponent,
	InputComponent,
	BackComponent,
	ErrorInputComponent,
	HeaderComponent,
	NotifComponent,
	HeaderListHome,
	ContentListLatest,
	ContentListPopular,
	ContentListAll,
	NotifSuccessComponent,
	NodataComponent,
	ButtonChapterComponent,
	ContentHeaderDetail,
	ContentGenreDetail,
	ContentSynopsisDetail,
	ContentChapterDetail,
};
