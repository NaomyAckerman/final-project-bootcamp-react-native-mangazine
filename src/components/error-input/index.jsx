import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function index({ message }) {
	return <Text style={styles.inputError}>{message}</Text>;
}

const styles = StyleSheet.create({
	inputError: {
		color: 'red',
		marginTop: 2,
		paddingHorizontal: 25,
	},
});
