import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors } from '../../themes/colors';

export default function ({ section, navigation }) {
	return (
		<View style={[styles.header, section.bgSecondary && styles.bgSecondary]}>
			<View>
				<Text style={styles.title}>{section.title}</Text>
				<Text style={styles.desc}>{section.desc}</Text>
			</View>
			{section.showMore && (
				<Text
					style={styles.link}
					onPress={() =>
						navigation.navigate('List', {
							part: section.part,
						})
					}
				>
					Show More
				</Text>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'flex-end',
		paddingTop: 20,
		paddingBottom: 10,
		paddingHorizontal: 20,
	},
	title: {
		marginBottom: 3,
		fontSize: 20,
		color: colors.black,
	},
	desc: {
		fontSize: 15,
		color: colors.secondary,
	},
	link: {
		color: colors.blue,
	},
	bgSecondary: {
		backgroundColor: colors.lightBlue,
	},
});
