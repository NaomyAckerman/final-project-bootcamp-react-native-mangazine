import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../themes/colors';
import { FontAwesome } from '@expo/vector-icons';

export default function index({ info, outline = true, news, ...rest }) {
	return (
		<TouchableOpacity
			{...rest}
			style={[styles.buttonWrapper, outline ? styles.buttonSolid : '']}
			activeOpacity={0.8}
		>
			<FontAwesome
				name="book"
				size={24}
				color={outline ? colors.black : colors.white}
			/>
			<View style={styles.textWrapper}>
				<Text
					style={[styles.buttonText, outline ? styles.buttonTextSolid : '']}
				>
					{info.name}
				</Text>
				<Text
					style={[
						styles.buttonTextDate,
						outline ? styles.buttonTextDateSolid : '',
					]}
				>
					Release {info.date}
				</Text>
			</View>
			{news && <Text style={styles.newText}>New</Text>}
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	buttonWrapper: {
		flexDirection: 'row',
		paddingVertical: 10,
		paddingHorizontal: 25,
		borderRadius: 50,
		backgroundColor: colors.blue,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonText: {
		color: colors.white,
		fontWeight: 'bold',
		fontSize: 16,
		marginBottom: 3,
	},
	buttonTextDate: {
		color: colors.white,
		fontSize: 15,
	},
	buttonSolid: {
		backgroundColor: colors.white,
		borderWidth: 1,
		borderColor: colors.blueSecondary,
	},
	buttonTextSolid: {
		color: colors.blueSecondary,
	},
	buttonTextDateSolid: {
		color: colors.secondary,
		fontSize: 15,
	},
	textWrapper: {
		flex: 1,
		paddingLeft: 15,
	},
	newText: {
		alignSelf: 'flex-start',
		fontSize: 16,
		fontWeight: 'bold',
		color: 'yellow',
	},
});
