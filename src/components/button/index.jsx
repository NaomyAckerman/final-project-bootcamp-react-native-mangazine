import React from 'react';
import {
	ActivityIndicator,
	StyleSheet,
	Text,
	TouchableOpacity,
} from 'react-native';
import { colors } from '../../themes/colors';

export default function index({ title, outline, loading, ...rest }) {
	return (
		<TouchableOpacity
			{...rest}
			style={[styles.buttonWrapper, outline ? styles.buttonSolid : '']}
			disabled={loading}
		>
			{loading && (
				<ActivityIndicator color={colors.white} style={styles.load} />
			)}
			<Text style={[styles.buttonText, outline ? styles.buttonTextSolid : '']}>
				{title}
			</Text>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	buttonWrapper: {
		flexDirection: 'row',
		paddingVertical: 18,
		borderRadius: 50,
		backgroundColor: colors.blue,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonText: {
		color: colors.white,
		fontWeight: 'bold',
		fontSize: 15,
	},
	buttonSolid: {
		backgroundColor: colors.white,
		borderWidth: 1,
		borderColor: colors.blueSecondary,
	},
	buttonTextSolid: {
		color: colors.blueSecondary,
	},
	load: {
		marginRight: 5,
	},
});
