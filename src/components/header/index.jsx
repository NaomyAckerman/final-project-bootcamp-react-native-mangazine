import React from 'react';
import { Feather } from '@expo/vector-icons';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SvgCssUri } from 'react-native-svg';
import { colors } from '../../themes/colors';
import { util } from '../../utils';

export default function index({ diplayName, navigation }) {
	return (
		<View style={styles.box}>
			<View style={styles.header}>
				<TouchableOpacity
					activeOpacity={0.8}
					onPress={() => navigation.navigate('Profile')}
				>
					<SvgCssUri
						uri={`https://avatars.dicebear.com/api/adventurer-neutral/${diplayName}.svg?r=50&flip=1`}
						style={styles.headerImage}
					/>
				</TouchableOpacity>
				<View style={styles.profile}>
					<Text style={styles.profileGreeting}>{util.greeting()}</Text>
					<Text style={styles.profileName}>{diplayName}</Text>
				</View>
				<Feather
					name="search"
					size={24}
					color={colors.black}
					style={styles.headerIcon}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	box: {
		position: 'absolute',
		top: 0,
		width: '100%',
		overflow: 'hidden',
		paddingBottom: 5,
		marginTop: 24,
	},
	header: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: colors.white,
		paddingVertical: 7,
		paddingHorizontal: 20,
		shadowColor: colors.secondary,
		shadowOffset: { width: 0, height: 1 },
		shadowRadius: 0.25,
		shadowOpacity: 0.25,
		elevation: 4,
	},
	headerImage: {
		height: 45,
		width: 45,
	},
	profile: {
		flex: 1,
		height: '100%',
		paddingLeft: 10,
		justifyContent: 'center',
	},
	profileGreeting: {
		marginBottom: 3,
		color: colors.secondary,
	},
	profileName: {
		fontSize: 15,
		color: colors.black,
	},
	headerIcon: {
		textAlignVertical: 'center',
		height: '100%',
	},
});
