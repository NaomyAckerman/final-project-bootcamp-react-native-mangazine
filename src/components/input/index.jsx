import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { AntDesign, Feather } from '@expo/vector-icons';
import { colors } from '../../themes/colors';

export default function index({
	type = 'user',
	onpressSecure,
	secure,
	...rest
}) {
	return (
		<View style={styles.formInput}>
			<AntDesign
				name={type === 'email' ? 'mail' : type === 'password' ? 'key' : 'user'}
				size={24}
				color={colors.black}
			/>
			<TextInput
				style={styles.textInput}
				{...rest}
				placeholderTextColor={colors.secondary}
				selectionColor={colors.blueTint}
				secureTextEntry={secure}
			/>
			{type === 'password' && (
				<Feather
					onPress={onpressSecure}
					name={secure ? 'eye-off' : 'eye'}
					size={24}
					color={colors.black}
					style={styles.iconSecure}
				/>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	formInput: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 25,
		paddingVertical: 13,
		borderWidth: 1,
		borderRadius: 50,
		borderColor: colors.blueSecondary,
		backgroundColor: colors.white,
	},
	textInput: {
		flex: 1,
		fontSize: 15,
		marginLeft: 10,
		color: colors.blueSecondary,
	},
	iconSecure: {
		marginLeft: 10,
	},
});
