import React from 'react';
import { colors } from '../../themes/colors';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { Feather } from '@expo/vector-icons';

export default function index({ title, description }) {
	return (
		<SafeAreaView style={styles.safeArea}>
			<Feather
				name="alert-triangle"
				size={24}
				color={colors.error}
				style={styles.icon}
			/>
			<View style={styles.container}>
				<Text style={styles.title}>{title}</Text>
				<Text style={styles.description}>{description}</Text>
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	safeArea: {
		flexDirection: 'row',
		elevation: 3,
		marginTop: 25,
		marginHorizontal: 10,
		borderRadius: 7,
		backgroundColor: colors.white,
		padding: 20,
		alignItems: 'center',
	},
	container: {
		flex: 1,
		alignItems: 'center',
	},
	icon: {
		alignSelf: 'center',
	},
	title: {
		fontSize: 16,
		color: colors.error,
		fontWeight: 'bold',
	},
	description: {
		fontSize: 15,
		color: colors.black,
		fontWeight: '500',
	},
});
