export const colors = {
	blue: '#4457FF',
	blueSecondary: '#485CC7',
	blueTint: '#BCC2FF',
	lightBlue: '#F0F4FF',
	darkBlue: '#0D2292',
	secondary: '#828899',
	black: '#141414',
	blackSpace: '#2F2E41',
	white: '#ffffff',
	error: '#f25050',
	success: '#51e886',
	heart: '#E31B23',
};
