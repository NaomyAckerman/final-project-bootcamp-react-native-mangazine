import * as Yup from 'yup';
const LoginSchema = Yup.object().shape({
	email: Yup.string().email().required().trim().label('Email'),
	password: Yup.string().required().trim().label('password'),
});

const updateSchema = Yup.object().shape({
	username: Yup.string().required().trim().label('Username'),
	password: Yup.string().min(6).max(20).trim().label('password'),
});

const RegisterSchema = Yup.object().shape({
	username: Yup.string().required().trim().label('username'),
	email: Yup.string().email().required().trim().label('Email'),
	password: Yup.string().required().min(6).max(20).trim().label('password'),
	confirmPassword: Yup.string()
		.required()
		.oneOf([Yup.ref('password')], `Confirm password dosn't match`)
		.label('Confirm password'),
});

export { LoginSchema, RegisterSchema, updateSchema };
