import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { registerImage } from '../../../assets';
import { colors } from '../../../themes/colors';
import { RegisterSchema } from '../../../validation';
import { Formik } from 'formik';
import { Notifier } from 'react-native-notifier';
import { Easing } from 'react-native-reanimated';
import { util } from '../../../utils';
import { registerUser } from '../../../redux/features/auth/authSlice';
import {
	Image,
	SafeAreaView,
	ScrollView,
	StyleSheet,
	Text,
	View,
} from 'react-native';
import {
	BackComponent,
	ButtonComponent,
	ErrorInputComponent,
	GapComponent,
	InputComponent,
	NotifComponent,
} from '../../../components';

export default function index({ navigation }) {
	const state = useSelector((state) => state.auth);
	const dispatch = useDispatch();
	const [secure, setSecure] = useState({
		password: true,
		confirm: true,
	});
	const [loading, setLoading] = useState(false);

	const handlerRegister = (values, { resetForm }) => {
		setLoading(true);
		dispatch(registerUser(values))
			.unwrap()
			.then((_res) => {
				resetForm();
			})
			.catch((err) => {
				Notifier.showNotification({
					title: 'Error Authentication',
					description: util.errorAuth(err.code),
					Component: NotifComponent,
					duration: 6000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView
				showsVerticalScrollIndicator={false}
				contentContainerStyle={styles.scrollWrapper}
			>
				<View style={styles.buttonBackWrapper}>
					<BackComponent onPress={() => navigation.popToTop()} />
				</View>
				<View style={styles.register}>
					<Text style={styles.registerTitle}>Register</Text>
					<Text style={styles.registerDesc}>
						Find the latest and most interesting collection of manhwa only here!
					</Text>
					<Image source={registerImage} style={styles.registerIllustration} />
				</View>
				<View style={styles.form}>
					<Formik
						initialValues={state}
						validationSchema={RegisterSchema}
						onSubmit={handlerRegister}
					>
						{({ errors, values, handleSubmit, handleChange, handleBlur }) => (
							<>
								<InputComponent
									type="user"
									placeholder="Your Username"
									autoComplete="username"
									onChangeText={handleChange('username')}
									onBlur={handleBlur('username')}
									value={values.username}
								/>
								{errors.username && (
									<ErrorInputComponent message={errors.username} />
								)}
								<GapComponent height={20} />
								<InputComponent
									type="email"
									placeholder="Your Email"
									autoComplete="email"
									keyboardType="email-address"
									onChangeText={handleChange('email')}
									onBlur={handleBlur('email')}
									value={values.email}
								/>
								{errors.email && <ErrorInputComponent message={errors.email} />}
								<GapComponent height={20} />
								<InputComponent
									type="password"
									placeholder="Your Password"
									secure={secure.password}
									autoComplete="password"
									onpressSecure={() =>
										setSecure({ ...secure, ['password']: !secure.password })
									}
									onChangeText={handleChange('password')}
									onBlur={handleBlur('password')}
									value={values.password}
								/>
								{errors.password && (
									<ErrorInputComponent message={errors.password} />
								)}
								<GapComponent height={20} />
								<InputComponent
									type="password"
									placeholder="Your Confirm Password"
									secure={secure.confirm}
									autoComplete="password"
									onpressSecure={() =>
										setSecure({ ...secure, ['confirm']: !secure.confirm })
									}
									onChangeText={handleChange('confirmPassword')}
									onBlur={handleBlur('confirmPassword')}
									value={values.confirmPassword}
								/>
								{errors.confirmPassword && (
									<ErrorInputComponent message={errors.confirmPassword} />
								)}
								<GapComponent height={40} />
								<ButtonComponent
									title="Register"
									onPress={handleSubmit}
									loading={loading}
								/>
							</>
						)}
					</Formik>
				</View>
				<View style={styles.footerWrapper}>
					<Text style={styles.footer}>
						Already have an account?{' '}
						<Text
							style={styles.footerNavigation}
							onPress={() => navigation.navigate('Login')}
						>
							Please login here!
						</Text>
					</Text>
				</View>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.white,
		paddingTop: 24,
	},
	scrollWrapper: {
		paddingHorizontal: 25,
	},
	buttonBackWrapper: {
		marginTop: 30,
	},
	register: {
		marginTop: 50,
	},
	registerTitle: {
		fontSize: 27,
		marginBottom: 10,
		fontWeight: 'bold',
		color: colors.darkBlue,
	},
	registerDesc: {
		width: '60%',
		fontSize: 15,
		marginBottom: 20,
		color: colors.secondary,
		textAlign: 'left',
	},
	registerIllustration: {
		width: 340,
		height: 210,
		resizeMode: 'cover',
	},
	form: {
		marginTop: 37,
	},
	footer: {
		fontSize: 15,
		marginVertical: 20,
		color: colors.secondary,
		textAlign: 'center',
	},
	footerNavigation: {
		color: colors.blue,
	},
});
