import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../themes/colors';
import { loginImage } from '../../../assets';
import { LoginSchema } from '../../../validation';
import { Formik } from 'formik';
import { Notifier } from 'react-native-notifier';
import { Easing } from 'react-native-reanimated';
import { util } from '../../../utils';
import { loginUser } from '../../../redux/features/auth/authSlice';
import {
	Image,
	SafeAreaView,
	StyleSheet,
	Text,
	View,
	ScrollView,
} from 'react-native';
import {
	BackComponent,
	ButtonComponent,
	ErrorInputComponent,
	GapComponent,
	InputComponent,
	NotifComponent,
} from '../../../components';

export default function index({ navigation }) {
	const state = useSelector((state) => state.auth);
	const dispatch = useDispatch();
	const [secure, setSecure] = useState(true);
	const [loading, setLoading] = useState(false);

	const handlerLogin = (values, { resetForm }) => {
		setLoading(true);
		dispatch(loginUser(values))
			.unwrap()
			.then((_res) => {
				resetForm();
			})
			.catch((err) => {
				Notifier.showNotification({
					title: 'Error Authentication',
					description: util.errorAuth(err.code),
					Component: NotifComponent,
					duration: 6000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView
				showsVerticalScrollIndicator={false}
				contentContainerStyle={styles.scrollWrapper}
			>
				<View style={styles.buttonBackWrapper}>
					<BackComponent onPress={() => navigation.popToTop()} />
				</View>
				<View style={styles.login}>
					<Text style={styles.loginTitle}>Login</Text>
					<Text style={styles.loginDesc}>
						Find the latest and most interesting collection of manhwa only here!
					</Text>
					<Image source={loginImage} style={styles.loginIllustration} />
				</View>
				<View style={styles.form}>
					<Formik
						initialValues={state}
						validationSchema={LoginSchema}
						onSubmit={handlerLogin}
					>
						{({ errors, values, handleSubmit, handleChange, handleBlur }) => (
							<>
								<InputComponent
									type="email"
									placeholder="Your Email"
									autoComplete="email"
									keyboardType="email-address"
									onChangeText={handleChange('email')}
									onBlur={handleBlur('email')}
									value={values.email}
								/>
								{errors.email && <ErrorInputComponent message={errors.email} />}
								<GapComponent height={20} />
								<InputComponent
									type="password"
									placeholder="Your Password"
									secure={secure}
									autoComplete="password"
									onpressSecure={() => setSecure(!secure)}
									onChangeText={handleChange('password')}
									onBlur={handleBlur('password')}
									value={values.password}
								/>
								{errors.password && (
									<ErrorInputComponent message={errors.password} />
								)}
								<GapComponent height={40} />
								<ButtonComponent
									title="Login"
									onPress={handleSubmit}
									loading={loading}
								/>
							</>
						)}
					</Formik>
				</View>
				<Text style={styles.footer}>
					Don’t have an account?{' '}
					<Text
						style={styles.footerNavigation}
						onPress={() => navigation.navigate('Register')}
					>
						Register here!
					</Text>
				</Text>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.white,
		paddingTop: 24,
	},
	scrollWrapper: {
		paddingHorizontal: 25,
	},
	buttonBackWrapper: {
		marginTop: 30,
	},
	login: {
		marginTop: 50,
	},
	loginTitle: {
		fontSize: 27,
		fontWeight: 'bold',
		marginBottom: 10,
		color: colors.darkBlue,
	},
	loginDesc: {
		width: '60%',
		marginTop: 10,
		color: colors.secondary,
		textAlign: 'left',
	},
	loginIllustration: {
		width: 230,
		height: 210,
		marginTop: 37,
		resizeMode: 'cover',
	},
	form: {
		marginTop: 37,
	},
	footer: {
		fontSize: 15,
		marginVertical: 20,
		color: colors.secondary,
		textAlign: 'center',
	},
	footerNavigation: {
		color: colors.blue,
	},
});
