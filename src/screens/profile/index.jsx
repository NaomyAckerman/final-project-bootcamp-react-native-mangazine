import React, { useState } from 'react';
import { SvgCssUri } from 'react-native-svg';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUser, editUser } from '../../redux/features/auth/authSlice';
import { colors } from '../../themes/colors';
import { MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';
import { Formik } from 'formik';
import { updateSchema } from '../../validation';
import { Notifier } from 'react-native-notifier';
import { util } from '../../utils';
import { Easing } from 'react-native-reanimated';
import {
	ButtonComponent,
	ErrorInputComponent,
	GapComponent,
	InputComponent,
	NotifComponent,
	NotifSuccessComponent,
} from '../../components';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';

export default function index() {
	const dispatch = useDispatch();
	const state = useSelector((state) => state.auth);
	const { favorite } = useSelector((state) => state.manhwa);
	const [secure, setSecure] = useState(true);
	const [loading, setLoading] = useState(false);

	const handlerUpdate = (values, { resetForm }) => {
		setLoading(true);
		dispatch(editUser(values))
			.unwrap()
			.then((res) => {
				Notifier.showNotification({
					title: 'Change Profile',
					description: res.message,
					Component: NotifSuccessComponent,
					duration: 3000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			})
			.catch((err) => {
				Notifier.showNotification({
					title: 'Error Change Profile',
					description: util.errorAuth(err.code),
					Component: NotifComponent,
					duration: 6000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			})
			.finally(() => {
				resetForm({ values: { ...values, password: '' } });
				setLoading(false);
			});
	};

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={styles.bg}>
					<View style={styles.ring1}></View>
					<View style={styles.ring2}></View>
				</View>
				<View style={styles.boxWrapper}>
					<View style={styles.boxImageProfile}>
						<SvgCssUri
							uri={`https://avatars.dicebear.com/api/adventurer-neutral/${state.username}.svg?r=50&flip=1`}
							style={styles.imageProfile}
						/>
					</View>
				</View>
				<View style={styles.profileWrapper}>
					<Text style={styles.title}>{state.username}</Text>
					<View style={styles.cardSocialMedia}>
						<View style={styles.socialWrapper}>
							<MaterialCommunityIcons name="gmail" size={24} color="#4285F4" />
							<Text style={styles.textSocial}>{state.email}</Text>
						</View>
						<GapComponent height={15} />
						<View style={styles.socialWrapper}>
							<AntDesign name="heart" size={24} color="#E31B23" />
							<Text style={styles.textSocial}>
								My favorite comic {favorite.length}
							</Text>
						</View>
					</View>
					<Text style={styles.title}>Edit Profile</Text>
					<Formik
						initialValues={state}
						validationSchema={updateSchema}
						onSubmit={handlerUpdate}
					>
						{({ errors, values, handleSubmit, handleChange, handleBlur }) => (
							<>
								<InputComponent
									type="user"
									placeholder="Your username"
									onChangeText={handleChange('username')}
									onBlur={handleBlur('username')}
									value={values.username}
								/>
								{errors.username && (
									<ErrorInputComponent message={errors.username} />
								)}
								<GapComponent height={20} />
								<InputComponent
									type="password"
									placeholder="Your change password"
									secure={secure}
									autoComplete="password"
									onpressSecure={() => setSecure(!secure)}
									onChangeText={handleChange('password')}
									onBlur={handleBlur('password')}
									value={values.password}
								/>
								{errors.password && (
									<ErrorInputComponent message={errors.password} />
								)}
								<GapComponent height={40} />
								<ButtonComponent
									title="Change Profile"
									onPress={handleSubmit}
									loading={loading}
								/>
								<GapComponent height={30} />
								<ButtonComponent
									title="Logout"
									onPress={() => {
										dispatch(logoutUser());
									}}
									outline={true}
								/>
							</>
						)}
					</Formik>
				</View>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	bg: {
		height: 220,
		borderBottomEndRadius: 43,
		borderBottomStartRadius: 43,
		backgroundColor: colors.blueSecondary,
		position: 'relative',
		overflow: 'hidden',
	},
	ring1: {
		height: 150,
		width: 150,
		borderRadius: 150,
		top: -40,
		right: -50,
		position: 'absolute',
		backgroundColor: '#3D42C6',
	},
	ring2: {
		height: 70,
		width: 70,
		borderRadius: 150,
		top: 80,
		right: 140,
		position: 'absolute',
		backgroundColor: '#3D42C6',
	},
	imageProfile: {
		height: 140,
		width: 140,
		borderRadius: 35,
	},
	boxWrapper: {
		position: 'relative',
		height: 85,
	},
	boxImageProfile: {
		position: 'absolute',
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		width: 155,
		height: 155,
		borderRadius: 35,
		top: -70,
		backgroundColor: colors.white,
		elevation: 2,
	},
	title: {
		marginVertical: 20,
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center',
	},
	profileWrapper: {
		paddingHorizontal: 23,
		paddingBottom: 20,
	},
	cardSocialMedia: {
		elevation: 2,
		paddingVertical: 25,
		paddingHorizontal: 30,
		backgroundColor: colors.white,
		borderRadius: 25,
	},
	socialWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	textSocial: {
		fontSize: 17,
		marginLeft: 15,
	},
});
