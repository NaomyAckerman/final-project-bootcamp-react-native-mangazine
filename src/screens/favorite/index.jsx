import React, { useState, useEffect } from 'react';
import {
	ActivityIndicator,
	FlatList,
	Image,
	SafeAreaView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import {
	GapComponent,
	HeaderComponent,
	NodataComponent,
	NotifSuccessComponent,
} from '../../components';
import {
	deleteFavorite,
	getFavorite,
	reset,
} from '../../redux/features/manhwa/manhwaSlice';
import { Entypo } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { util } from '../../utils';
import { colors } from '../../themes/colors';
import { Notifier } from 'react-native-notifier';
import { Easing } from 'react-native-reanimated';

const RenderContent = ({ item, onDelete, onRead, navigation }) => {
	return (
		<View style={styles.content}>
			<TouchableOpacity
				activeOpacity={0.5}
				onPress={() =>
					navigation.navigate('Detail', { endpoint: item.endpoint })
				}
			>
				<LinearGradient
					colors={['rgba(0,0,0,0.2)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,0.6)']}
					style={styles.background}
				/>
				<Image source={{ uri: item.thumbnail }} style={styles.imageContent} />
			</TouchableOpacity>
			<Text style={styles.titleText}>{util.sliceText(item.title, 35)}</Text>
			<Text style={styles.titleLatest}>Latest Read {item.latest_read}</Text>
			<TouchableOpacity
				activeOpacity={0.8}
				style={styles.detailIcon}
				onPress={onDelete}
			>
				<Entypo name="trash" size={24} color={colors.error} />
			</TouchableOpacity>
			{item.latest_read !== '-' && (
				<Text style={styles.readLatest} onPress={onRead}>
					Read now
				</Text>
			)}
		</View>
	);
};

const RenderHeader = () => {
	return <Text style={styles.titleHeader}>My Favorite Manhwa</Text>;
};

export default function index({ navigation }) {
	const dispatch = useDispatch();
	const { token: uid, username } = useSelector((state) => state.auth);
	const { favorite, detail } = useSelector((state) => state.manhwa);
	const [fetchStatus, setFetchStatus] = useState(false);

	const deleteHandler = ({ id, uid }) => {
		dispatch(deleteFavorite({ id, uid }))
			.unwrap()
			.then((res) => {
				Notifier.showNotification({
					title: 'Delete favorite comics',
					description: res.message,
					Component: NotifSuccessComponent,
					duration: 3000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			})
			.catch((err) => {
				Notifier.showNotification({
					title: 'Error Delete Favorite',
					description: util.errorAuth(err.code),
					Component: NotifComponent,
					duration: 6000,
					showAnimationDuration: 500,
					showEasing: Easing.bounce,
					hideOnPress: false,
				});
			});
	};

	const readHandler = ({ title, latest_read_endpoint: endpoint, type }) => {
		dispatch(getFavorite({ uid, title }));
		navigation.navigate('Read', { endpoint, type });
	};

	useEffect(() => {
		if (fetchStatus) {
			dispatch(getFavorite({ uid }));
		}
		setFetchStatus(false);
		return () =>
			navigation.addListener('focus', () => {
				dispatch(reset({ key: 'detail', value: {} }));
				dispatch(reset({ key: 'favoriteDetail', value: {} }));
			});
	}, [fetchStatus]);

	return (
		<SafeAreaView style={styles.container}>
			<HeaderComponent diplayName={username} navigation={navigation} />
			<FlatList
				data={favorite}
				ListHeaderComponent={() => <RenderHeader />}
				renderItem={({ item }) => (
					<RenderContent
						item={item}
						onDelete={() => deleteHandler(item)}
						onRead={() => readHandler(item)}
						navigation={navigation}
					/>
				)}
				ListEmptyComponent={() =>
					fetchStatus ? (
						<ActivityIndicator
							size="large"
							color={colors.blue}
							style={{ marginTop: 300 }}
						/>
					) : (
						<NodataComponent title="No favorite comics" />
					)
				}
				ItemSeparatorComponent={() => <GapComponent height={15} />}
				style={styles.scrollWrapper}
				contentContainerStyle={styles.contentContainer}
				keyExtractor={(_item, index) => index.toString()}
				onRefresh={() => setFetchStatus(true)}
				refreshing={false}
				showsVerticalScrollIndicator={false}
			/>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	contentContainer: {
		paddingBottom: 20,
	},
	scrollWrapper: {
		width: '100%',
		marginTop: 84,
		paddingHorizontal: 25,
	},
	titleHeader: {
		fontSize: 20,
		marginVertical: 20,
		textAlign: 'center',
	},
	content: {
		position: 'relative',
		height: 230,
		borderRadius: 25,
		overflow: 'hidden',
	},
	background: {
		zIndex: 2,
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
	},
	imageContent: {
		height: 450,
	},
	titleText: {
		position: 'absolute',
		fontSize: 20,
		fontWeight: 'bold',
		color: colors.white,
		top: 26,
		left: 25,
	},
	titleLatest: {
		position: 'absolute',
		fontWeight: '500',
		fontSize: 16,
		color: colors.white,
		bottom: 20,
		left: 25,
	},
	readLatest: {
		color: colors.white,
		position: 'absolute',
		bottom: 20,
		right: 25,
		fontSize: 16,
	},
	detailIcon: {
		backgroundColor: colors.white,
		position: 'absolute',
		padding: 7,
		borderRadius: 50,
		top: 20,
		right: 25,
	},
});
