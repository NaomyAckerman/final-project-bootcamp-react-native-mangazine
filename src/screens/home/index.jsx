import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, SafeAreaView, SectionList, View } from 'react-native';
import {
	ContentListAll,
	ContentListLatest,
	ContentListPopular,
	HeaderComponent,
	HeaderListHome,
} from '../../components';
import {
	getAll,
	getFavorite,
	getGenre,
	getNewRelease,
	getPopular,
	reset,
} from '../../redux/features/manhwa/manhwaSlice';

const RenderHeaderSectionList = ({ section, navigation }) => {
	return (
		<View>
			<HeaderListHome section={section} navigation={navigation} />
			{section.part === 'latest' && (
				<ContentListLatest
					item={section.data}
					section={section}
					navigation={navigation}
				/>
			)}
			{section.part === 'popular' && (
				<ContentListPopular
					item={section.data}
					section={section}
					navigation={navigation}
				/>
			)}
			{section.part === 'list' && (
				<ContentListAll
					item={section.data}
					section={section}
					navigation={navigation}
				/>
			)}
		</View>
	);
};

export default function index({ navigation }) {
	const dispatch = useDispatch();
	const { latest, popular, list } = useSelector((state) => state.manhwa);
	const { token: uid, username } = useSelector((state) => state.auth);
	const [fetchStatus, setFetchStatus] = useState(true);

	useEffect(() => {
		if (fetchStatus) {
			dispatch(getGenre({}));
			dispatch(getNewRelease({ page: 1, count: 7 }));
			dispatch(getPopular({ date: '?date=weekly' }));
			dispatch(getAll({ page: 1 }));
			dispatch(getFavorite({ uid }));
		}
		setFetchStatus(false);
		return () =>
			navigation.addListener('focus', () => {
				dispatch(reset({ key: 'detail', value: {} }));
				dispatch(reset({ key: 'favoriteDetail', value: {} }));
			});
	}, [fetchStatus]);

	const dataSection = [
		{
			title: 'New Release!',
			part: 'latest',
			desc: 'Read the latest manhwa recomendation!',
			data: latest,
			showMore: true,
		},
		{
			title: 'Most Popular!',
			part: 'popular',
			desc: 'Lots of interesting manhwa this week!',
			data: popular.weekly,
			showMore: false,
			bgSecondary: true,
		},
		{
			title: 'Collection List!',
			part: 'list',
			desc: 'Interesting manhwa collection here!',
			data: list,
			showMore: true,
		},
	];

	return (
		<SafeAreaView style={styles.container}>
			<HeaderComponent diplayName={username} navigation={navigation} />
			<SectionList
				sections={dataSection}
				stickySectionHeadersEnabled={false}
				renderSectionHeader={({ section }) => (
					<RenderHeaderSectionList section={section} navigation={navigation} />
				)}
				renderItem={() => <View />}
				keyExtractor={(_item, index) => index.toString()}
				style={styles.scrollWrapper}
				onRefresh={() => setFetchStatus(true)}
				refreshing={false}
			/>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	scrollWrapper: {
		flex: 1,
		width: '100%',
		marginTop: 84,
	},
});
