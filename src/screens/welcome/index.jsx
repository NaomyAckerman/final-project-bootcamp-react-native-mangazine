import React from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { ButtonComponent, GapComponent } from '../../components';
import { colors } from '../../themes/colors';
import { welcomeImage } from '../../assets';

export default function index({ navigation }) {
	return (
		<SafeAreaView style={styles.container}>
			<View style={styles.welcome}>
				<Image source={welcomeImage} style={styles.welcomeIllustration} />
				<Text style={styles.welcomeTitle}>Welcome to MangaZine</Text>
				<Text style={styles.welcomeDesc}>
					Hurry up and read a collection of manhwa which is always updated every
					day
				</Text>
			</View>
			<View style={styles.buttonAuth}>
				<ButtonComponent
					title="Login Now"
					outline={true}
					onPress={() => navigation.navigate('Login')}
				/>
				<GapComponent height={20} />
				<ButtonComponent
					title="Register Now"
					onPress={() => navigation.navigate('Register')}
				/>
			</View>
			<Text style={styles.footer}>Created by Novyan Dicky Prasetyo</Text>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 25,
		backgroundColor: colors.white,
		paddingTop: 24,
	},
	welcome: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	welcomeIllustration: {
		width: 260,
		height: 260,
		resizeMode: 'cover',
	},
	welcomeTitle: {
		fontSize: 27,
		fontWeight: 'bold',
		marginBottom: 10,
		color: colors.darkBlue,
	},
	welcomeDesc: {
		width: 205,
		fontSize: 15,
		color: colors.secondary,
		textAlign: 'center',
	},
	buttonAuth: {
		flex: 1,
		justifyContent: 'center',
	},
	footer: {
		fontSize: 15,
		marginBottom: 20,
		color: colors.secondary,
		textAlign: 'center',
	},
});
