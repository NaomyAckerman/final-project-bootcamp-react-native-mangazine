import React, { useEffect, useState } from 'react';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { colors } from '../../themes/colors';
import { useDispatch, useSelector } from 'react-redux';
import {
	getFavorite,
	getRead,
	reset,
} from '../../redux/features/manhwa/manhwaSlice';
import {
	ActivityIndicator,
	Dimensions,
	FlatList,
	Image,
	SafeAreaView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';

const { width } = Dimensions.get('window');

const Header = ({ read, onBack, onNext, onPrev, nextLoading, prevLoading }) => {
	return (
		<>
			<View style={styles.buttonWrapper}>
				<View style={styles.wrapperContent}>
					<TouchableOpacity style={[styles.button, styles.count]}>
						<Text style={styles.text}>{read.images.length} Page</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.back} onPress={onBack}>
						<AntDesign name="back" size={20} color="white" />
					</TouchableOpacity>
				</View>

				<View style={[styles.wrapperContent, styles.right]}>
					<TouchableOpacity
						style={[
							styles.prev,
							styles.button,
							read.prevChapter === '-' ? styles.inactive : styles.active,
						]}
						onPress={onPrev}
						disabled={read.prevChapter === '-' ? true : prevLoading}
					>
						{!prevLoading ? (
							<Ionicons
								name="chevron-back"
								size={20}
								color={read.prevChapter === '-' ? colors.blue : colors.white}
							/>
						) : (
							<ActivityIndicator
								style={{ marginRight: 3 }}
								size="small"
								color={read.prevChapter === '-' ? colors.blue : colors.white}
							/>
						)}

						<Text
							style={[
								styles.text,
								read.prevChapter === '-'
									? styles.textInactive
									: styles.textActive,
							]}
						>
							Prev
						</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							styles.button,
							read.nextChapter === '-' ? styles.inactive : styles.active,
						]}
						onPress={onNext}
						disabled={read.nextChapter === '-' ? true : nextLoading}
					>
						<Text
							style={[
								styles.text,
								read.nextChapter === '-'
									? styles.textInactive
									: styles.textActive,
							]}
						>
							Next
						</Text>
						{!nextLoading ? (
							<Ionicons
								name="md-chevron-forward-sharp"
								size={20}
								color={read.nextChapter === '-' ? colors.blue : colors.white}
							/>
						) : (
							<ActivityIndicator
								style={{ marginLeft: 3 }}
								size="small"
								color={read.nextChapter === '-' ? colors.blue : colors.white}
							/>
						)}
					</TouchableOpacity>
				</View>
			</View>
			<Text style={styles.title}>{read.title}</Text>
		</>
	);
};

export default function index({ navigation, route }) {
	const dispatch = useDispatch();
	const { read } = useSelector((state) => state.manhwa);
	const { token: uid } = useSelector((state) => state.auth);
	const [fetchStatus, setFetchStatus] = useState(true);
	const [nextLoading, setNextLoading] = useState(false);
	const [prevLoading, setPrevLoading] = useState(false);
	const { endpoint, type } = route.params;
	let widthImage = width / (type === 'Manga' ? 1 : 1.5);

	useEffect(() => {
		if (fetchStatus) {
			dispatch(getRead({ endpoint }));
		}
		setFetchStatus(false);
		return () => {
			navigation.addListener('blur', () => {
				dispatch(reset({ key: 'read', value: {} }));
				dispatch(getFavorite({ uid }));
			});
		};
	}, [fetchStatus]);

	return (
		<SafeAreaView style={styles.container}>
			{fetchStatus || !read.images ? (
				<ActivityIndicator size="large" color={colors.blue} />
			) : (
				<>
					<FlatList
						data={read.images}
						renderItem={({ item, index }) => {
							return (
								// <View
								// 	style={{
								// 		flex: 1,
								// 		width,
								// 		height: '100%',
								// 		resizeMode: 'stretch',
								// 	}}
								// >
								// 	<Image
								// 		source={{ uri: item.url }}
								// 		style={{
								// 			width,
								// 			height: height / 2,
								// 			resizeMode: 'stretch',
								// 		}}
								// 	/>
								// </View>
								<View style={{ alignItems: 'center' }}>
									<AutoHeightImage
										resizeMethod="resize"
										width={widthImage}
										source={{ uri: item.url }}
									/>
								</View>
							);
						}}
						style={styles.scrollContainer}
						showsVerticalScrollIndicator={false}
						ListHeaderComponent={() => (
							<Header
								read={read}
								onBack={() => navigation.goBack()}
								nextLoading={nextLoading}
								onNext={() => {
									setNextLoading(true);
									dispatch(getRead({ endpoint: read.nextChapter })).finally(
										() => setNextLoading(false)
									);
								}}
								prevLoading={prevLoading}
								onPrev={() => {
									setPrevLoading(true);
									dispatch(getRead({ endpoint: read.prevChapter })).finally(
										() => setPrevLoading(false)
									);
								}}
							/>
						)}
						onRefresh={() => setFetchStatus(true)}
						refreshing={false}
						keyExtractor={(_item, index) => index.toString()}
					/>
				</>
			)}
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	scrollContainer: {
		width,
	},
	container: {
		flex: 1,
		paddingTop: 24,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.white,
	},
	title: {
		fontSize: 18,
		marginVertical: 20,
		paddingHorizontal: 35,
		textAlign: 'center',
	},
	buttonWrapper: {
		marginTop: 25,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	wrapperContent: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
	},
	right: {
		justifyContent: 'flex-end',
	},
	text: {
		fontSize: 15,
		fontWeight: 'bold',
		color: colors.white,
	},
	button: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10,
		paddingHorizontal: 15,
		borderRadius: 50,
	},
	count: {
		backgroundColor: colors.blackSpace,
	},
	back: {
		padding: 10,
		borderRadius: 20,
		marginLeft: 10,
		backgroundColor: colors.blackSpace,
	},
	prev: { marginRight: 10 },
	active: {
		backgroundColor: colors.blue,
	},
	inactive: {
		borderWidth: 1,
		borderColor: colors.blue,
		backgroundColor: colors.white,
	},
	textActive: {
		color: colors.white,
	},
	textInactive: {
		color: colors.blue,
	},
});
