import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	getDetail,
	deleteFavorite,
	setFavorite,
} from '../../redux/features/manhwa/manhwaSlice';
import {
	SafeAreaView,
	StyleSheet,
	View,
	SectionList,
	ActivityIndicator,
} from 'react-native';
import {
	ContentChapterDetail,
	ContentGenreDetail,
	ContentHeaderDetail,
	ContentSynopsisDetail,
	NotifComponent,
	NotifSuccessComponent,
} from '../../components';
import { colors } from '../../themes/colors';
import { Notifier } from 'react-native-notifier';
import { Easing } from 'react-native-reanimated';

export default function index({ navigation, route }) {
	const dispatch = useDispatch();
	const { detail, favoriteDetail } = useSelector((state) => state.manhwa);
	const { token: uid } = useSelector((state) => state.auth);
	const [fetchStatus, setFetchStatus] = useState(true);
	const [loading, setLoading] = useState(false);
	const { endpoint } = route.params;

	const handlerFavorite = ({ title, thumbnail, type }) => {
		setLoading(true);
		if (!favoriteDetail.title) {
			dispatch(
				setFavorite({
					uid,
					title,
					thumbnail,
					endpoint,
					type,
					latest_read: '-',
					latest_read_endpoint: '-',
				})
			)
				.unwrap()
				.then((res) => {
					Notifier.showNotification({
						title: 'Favorite',
						description: res.message,
						Component: NotifSuccessComponent,
						duration: 3000,
						showAnimationDuration: 500,
						showEasing: Easing.bounce,
						hideOnPress: false,
					});
				})
				.catch((_err) => {
					Notifier.showNotification({
						title: 'Error Favorite',
						description: 'Gagal',
						Component: NotifComponent,
						duration: 2000,
						showAnimationDuration: 500,
						showEasing: Easing.bounce,
						hideOnPress: false,
					});
				})
				.finally(() => {
					setLoading(false);
				});
		} else {
			dispatch(
				deleteFavorite({
					id: favoriteDetail.id,
					uid,
					title,
				})
			)
				.unwrap()
				.then((res) => {
					Notifier.showNotification({
						title: 'Favorite',
						description: res.message,
						Component: NotifSuccessComponent,
						duration: 3000,
						showAnimationDuration: 500,
						showEasing: Easing.bounce,
						hideOnPress: false,
					});
				})
				.catch((_err) => {
					Notifier.showNotification({
						title: 'Error Delete Favorite',
						description: 'Gagal',
						Component: NotifComponent,
						duration: 2000,
						showAnimationDuration: 500,
						showEasing: Easing.bounce,
						hideOnPress: false,
					});
				})
				.finally(() => {
					setLoading(false);
				});
		}
	};

	useEffect(() => {
		if (fetchStatus) {
			dispatch(getDetail({ endpoint, uid }));
		}
		setFetchStatus(false);
	}, [fetchStatus]);

	const dataSection = [
		{
			title: 'Header',
			part: 'header',
			data: [],
		},
		{
			title: 'Genre',
			part: 'genre',
			data: !detail.genres ? [] : detail.genres,
		},
		{
			title: 'Synopsis',
			part: 'synopsis',
			data: [],
		},
		{
			title: 'Chapter List',
			part: 'chapter_list',
			data: !detail.chapters ? [] : detail.chapters,
		},
	];

	return (
		<SafeAreaView style={styles.container}>
			{fetchStatus || !detail.title ? (
				<ActivityIndicator size="large" color={colors.blue} />
			) : (
				<SectionList
					onRefresh={() => setFetchStatus(true)}
					refreshing={false}
					sections={dataSection}
					stickySectionHeadersEnabled={false}
					renderItem={() => <View />}
					renderSectionHeader={({ section }) => {
						if (section.part === 'header') {
							return (
								<ContentHeaderDetail
									navigation={navigation}
									detail={detail}
									favorite={favoriteDetail}
									onFavorite={() => handlerFavorite(detail)}
									loading={loading}
								/>
							);
						}
						if (section.part === 'genre') {
							return <ContentGenreDetail data={section.data} />;
						}
						if (section.part === 'synopsis') {
							return <ContentSynopsisDetail detail={detail} />;
						}
						if (section.part === 'chapter_list') {
							return (
								<ContentChapterDetail
									chapter={section.data}
									onPress={({ item }) =>
										navigation.navigate('Read', {
											endpoint: item.endpoint,
											type: detail.type,
										})
									}
								/>
							);
						}
					}}
					keyExtractor={(_item, index) => index.toString()}
				/>
			)}

			{/* <ScrollView
				showsVerticalScrollIndicator={false}
				bounces={false}
				refreshControl={
					<RefreshControl
						refreshing={false}
						onRefresh={() => setFetchStatus(true)}
					/>
				}
			>
			<View style={styles.thumbnailWrapper}>
					<View style={styles.buttonBackWrapper}>
						<BackComponent
							border={false}
							color={colors.white}
							onPress={() => {
								navigation.goBack();
							}}
						/>
					</View>
					<LinearGradient
						colors={['rgba(0,0,0,0.1)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,0.8)']}
						style={styles.background}
					/>
					<Image
						source={{
							uri: detail.thumbnail,
						}}
						style={styles.thumbnail}
					/>
					<Text style={styles.title} numberOfLines={2}>
						{detail.title}
					</Text>
					{detail.title !== undefined && (
						<TouchableOpacity
							style={styles.favoriteWrapper}
							activeOpacity={0.7}
							onPress={() => handlerFavorite(detail)}
						>
							<AntDesign
								name={favorite.length ? 'heart' : 'hearto'}
								size={24}
								color="white"
							/>
						</TouchableOpacity>
					)}
				</View>
				<View style={styles.detailWrapper}>
					<Text style={styles.detailTitle}>{detail.title}</Text>
					<View style={styles.genreWrapper}>
						<Text style={styles.textTitle}>Genre</Text>
						<View style={styles.genre}>
							{detail.genres &&
								detail.genres.map((genre, index) => {
									return (
										<>
											<Text key={index.toString()} style={styles.genreText}>
												{genre.name}
											</Text>
											<GapComponent width={10} />
										</>
									);
								})}
						</View>
					</View>
					<View style={styles.synopsisWrapper}>
						<Text style={styles.textTitle}>Synopsis</Text>
						<Text style={styles.synopsisText}>{detail.synopsis}</Text>
					</View>
					<View style={styles.chapterWrapper}>
						<Text style={styles.textTitle}>Chapter List</Text>
						{detail.chapters &&
							detail.chapters.map((chapter, index) => {
								return (
									<>
										<GapComponent height={15} key={index.toString()} />
										<ButtonChapterComponent
											outline={index === 0 ? false : true}
											news={index === 0 ? true : false}
											info={chapter}
										/>
									</>
								);
							})}
					</View>
				</View>
			</ScrollView> */}
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.white,
	},
	detailWrapper: {
		paddingHorizontal: 30,
	},
	detailTitle: {
		fontSize: 17,
		textAlign: 'center',
		marginTop: 25,
		lineHeight: 22,
	},
});
