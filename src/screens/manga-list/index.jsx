import React, { useEffect, useState } from 'react';
import {
	FlatList,
	SafeAreaView,
	StyleSheet,
	Text,
	SectionList,
	Alert,
	View,
	ActivityIndicator,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ContentListAll } from '../../components';
import {
	getAll,
	getGenre,
	getNewRelease,
	reset,
} from '../../redux/features/manhwa/manhwaSlice';
import { colors } from '../../themes/colors';

export default function index({ navigation, route }) {
	const dispatch = useDispatch();
	const { listManhwa, genre, listGenre } = useSelector((state) => state.manhwa);
	const [fetchStatus, setFetchStatus] = useState(true);
	const [genreStatus, setGenreStatus] = useState('all');
	const [selectedId, setSelectedId] = useState(null);
	const [page, setPage] = useState(1);
	const { part } = route.params;

	useEffect(() => {
		if (fetchStatus) {
			genreStatus === 'all'
				? part === 'list'
					? dispatch(getAll({ page, count: 30, type: 'list' })).finally(() =>
							setFetchStatus(false)
					  )
					: dispatch(getNewRelease({ page, count: 30, type: 'list' })).finally(
							() => setFetchStatus(false)
					  )
				: dispatch(getGenre({ page, endpoint: genreStatus })).finally(() =>
						setFetchStatus(false)
				  );
		}
		return () => {
			navigation.addListener('focus', () => {
				dispatch(reset({ key: 'detail', value: {} }));
				dispatch(reset({ key: 'favoriteDetail', value: {} }));
			});
			navigation.addListener('blur', () => {
				dispatch(reset({ key: 'listManhwa', value: [] }));
				dispatch(reset({ key: 'listGenre', value: [] }));
			});
		};
	}, [fetchStatus]);

	const dataSection = [
		{
			key: 1,
			data: genre,
			horizontal: true,
		},
		{
			key: 2,
			data: genreStatus === 'all' ? listManhwa : listGenre,
			horizontal: false,
		},
	];

	return (
		<SafeAreaView style={styles.container}>
			<SectionList
				sections={dataSection}
				stickySectionHeadersEnabled={false}
				renderSectionHeader={({ section }) => {
					if (section.horizontal) {
						return (
							<FlatList
								data={section.data}
								renderItem={({ item }) => {
									return (
										<Text
											style={{
												paddingVertical: 10,
												paddingHorizontal: 15,
												backgroundColor: colors.blue,
												marginLeft: 10,
												color: colors.white,
												borderRadius: 25,
											}}
										>
											{item.name}
										</Text>
									);
								}}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
							/>
						);
					} else {
						return (
							<ContentListAll
								item={section.data}
								navigation={navigation}
								section={section}
							/>
						);
					}
				}}
				renderItem={() => <View />}
				ListFooterComponent={() =>
					fetchStatus && (
						<View
							style={{
								paddingVertical: 20,
								flex: 1,
								flexDirection: 'row',
								justifyContent: 'center',
								alignItems: 'center',
							}}
						>
							<ActivityIndicator size="small" color={colors.blue} />
							<Text style={{ marginLeft: 5 }}>Loading</Text>
						</View>
					)
				}
				keyExtractor={(_item, index) => index.toString()}
				style={styles.scrollWrapper}
				onRefresh={() => setFetchStatus(true)}
				refreshing={false}
				onEndReached={() => {
					setPage(page + 1);
					setFetchStatus(true);
				}}
				onEndReachedThreshold={2}
			/>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.white,
	},
	scrollWrapper: {
		flex: 1,
		width: '100%',
	},
});
