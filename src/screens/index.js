import WelcomeScreen from './welcome';
import LoginScreen from './auth/login';
import RegisterScreen from './auth/register';
import DetailScreen from './detail';
import HomeScreen from './home';
import MangaListScreen from './manga-list';
import SearchScreen from './search';
import ProfileScreen from './profile';
import FavoriteScreen from './favorite';
import ReadScreen from './read';
export {
	WelcomeScreen,
	LoginScreen,
	RegisterScreen,
	DetailScreen,
	HomeScreen,
	MangaListScreen,
	SearchScreen,
	ProfileScreen,
	FavoriteScreen,
	ReadScreen,
};
