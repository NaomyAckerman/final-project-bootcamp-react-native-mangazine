export const util = {
	errorAuth: (messaage) => {
		return messaage.replace(/-/g, ' ').replace('auth/', '');
	},
	sliceText: (sentence = '', count) =>
		`${sentence.substr(0, count)} ${
			sentence.length >= count ? '...' : ''
		}`.trim(),
	generateGenre: (genres) => {
		return genres.map((data) => data.genre).join(', ');
	},
	greeting: () => {
		let d = new Date();
		let time = d.getHours();
		let greeting;
		if (time >= 1) {
			greeting = 'Good Morning';
		}
		if (time === 12) {
			greeting = 'Good Noon';
		}
		if (time >= 14) {
			greeting = 'Good Afternoon';
		}
		if (time >= 18) {
			greeting = 'Good Evening';
		}
		return greeting;
	},
	replaceChapter: (chp, separator) => {
		return chp.replace(`${separator} `, '');
	},
};
