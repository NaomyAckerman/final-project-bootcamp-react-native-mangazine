import React from 'react';
import Navigation from './src/navigation';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import { NotifierWrapper } from 'react-native-notifier';
import { StatusBar } from 'expo-status-bar';

export default function App() {
	return (
		<Provider store={store}>
			<NotifierWrapper>
				<StatusBar style="auto" backgroundColor="transparent" />
				<Navigation />
			</NotifierWrapper>
		</Provider>
	);
}
